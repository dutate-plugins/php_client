<?php

namespace Dutate;

class Dutate {
    private $token;

    function __construct($token) {
        $this->token = $token;
    }

    function track($event,$extra=array()){
        $url = sprintf('https://api.dutate.com/track/event/%s/',$event);
        $options = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => "Content-type: application/x-www-form-urlencoded\n" . sprintf("Authorization: Bearer %s\r\n",$this->token),
                'content' => http_build_query($extra)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { 
            echo("ERROR");
        }
    }
}
?>
