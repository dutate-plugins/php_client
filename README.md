# elias/dutate-php

This library provides easy way to track your events to Dutate. 
It is simple to use and fully asyncronous.

## Installation
`composer require elias/dutate-php`

## Quick Start
```
require_once './vendor/autoload.php'
$dutate = new \Dutate\dutate("<your-token>");
$dutate->track("event name");
```
